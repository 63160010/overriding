/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class Child extends Kid {
    public Child(String name,int age){
        super(name,age);
        System.out.println("Child created");
        
    }
    //การเอาคำสั่งจากClass Kid มาเขียนคำสั่งใหม่ในรูปแบบเดิม
    @Override
    public void speak(){
        System.out.println("Child: "+name+" Age: "+age+" speak > Ngaa Ngaa!!! ");
    }
    
}
