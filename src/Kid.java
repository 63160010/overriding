/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class Kid {
    protected String name;
    protected int age;
    public Kid(String name,int age){
        System.out.println("KID created");
        this.name= name;
        this.age= age;
    }
    //
    public void speak(){
        System.out.println("KID speak");
        System.out.println("name: "+this.name );
        System.out.println("age: "+this.age );
    }
    
}
